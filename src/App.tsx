import React, {useState, useEffect, useCallback} from 'react';
import './App.css';

interface RandomUser {
  dob: {
    age : number
  }
}

function App() {

  const DURATION = 10000;

  const [isRunning, setIsRunning] = useState(true);
  const [averageYears, setAverageYears] = useState(-1);
  const [timeoutID, setTimeoutID] = useState(0);
  const [lastTimeoutTime, setLastTimeoutTime] = useState(0);
  const [stopTime, setStopTime] = useState(0);

  function StopButton() {
    window.clearTimeout(timeoutID);
    setStopTime(Date.now());
  }

  function StartButton() {
    setTimeoutID(window.setTimeout( Average,  lastTimeoutTime + DURATION - stopTime));
    setLastTimeoutTime(Date.now() - stopTime + lastTimeoutTime);
  }

  const Average = useCallback(() => {
    fetch("https://randomuser.me/api/?results=10")
      .then(res => res.json())
      .then(
        (result) => {
          var temp = 0;
          result.results.forEach((element: RandomUser) => {
            temp += element["dob"]["age"];
          });

          temp /= result.results.length;

          setAverageYears(temp);
        }
      )
    
    setTimeoutID(window.setTimeout(Average, DURATION));
    setLastTimeoutTime(Date.now());
  }, [])

  useEffect(() => {
    Average();
  }, [Average]);

  return (
    <div className="App">
      <header>
        <h1>Prosecan broj je {averageYears}</h1>
          <button onClick={() => {
            isRunning ? StopButton() : StartButton();
            setIsRunning(!isRunning);
          }}>
          <h3>{isRunning ? "Stop" : "Start"}</h3>
        </button>
      </header>
    </div>
  );
}

export default App;
